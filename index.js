const express = require("express")
const app = express()
const port = 3000 


app.set("view engine", "ejs")
app.use(express.urlencoded({extended: true}))
app.use("/", express.static("public"))
app.get("/hello", (req, res) => {
    let params = {
        name: req.query.name
    }
    res.render("hello", params)
})
app.get("/iniError", (req, res) => {
    res.send(hello)
})

app.get("/login", (req, res) => {
    let error = {
        message: req.query.error || "Please login"
    }
    res.render("login", error)
})

app.post("/login", (req, res) => {
    // logic ...
    try {
        let { email, password } = req.body

        if (email == "sabrina@mail.com" && password == "123456") {
            res.redirect("/game")
            return
        }
    } catch (e) {
        console.log(e.message)
    }

    res.redirect("/login?error=invalid%20username")
})

app.get("/game", (req, res) => {
   res.render('chpter4')
})

app.use((err, req, res, next) => {
    console.error(err)
    res.status(500).json({
        status: "Fail",
        errors: err.message
    })
})

app.use((req, res, next) => {
    res.status(404).json({
        status: "Fail",
        errors: "Not found"
    })
})

app.listen (port, () => (
    console.log(`express running on port ${port}`)
))
